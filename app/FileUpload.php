<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileUpload extends Model
{
    //
    protected $fillable = ['uuid', 'file_name', 'file'];
}
