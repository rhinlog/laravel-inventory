<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockInItems extends Model
{
    //
    protected $fillable = [
        'inventory_stock_no_id',
        'description',
        'unit',
        'quantity',
        'date_in',
        'received_by',
        'transferred_to',
        'type',
    ];

    public function ManageItems(){
        return $this->HasOne('App\ManageItems', 'id', 'inventory_stock_no_id');
    }
}
