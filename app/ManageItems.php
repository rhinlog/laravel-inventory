<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManageItems extends Model
{
    //
    protected $fillable = [
        'inventory_stock_no',
        'description',
        'unit',
        // 'quantity',
        'date_in',
        'stock_on_hand',
        'date_out',
        'type',
    ];

}
