<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryStockNoCounter extends Model
{
    //
    protected $fillable = [
        'last_no'
    ];
}
