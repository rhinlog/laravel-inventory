<?php

namespace App\Http\Controllers;
use App\ManageItems;
use App\InventoryStockNoCounter;
use App\User;
use Auth;
use Illuminate\Http\Request;

class OfficeSuppliesManageController extends Controller
{
    //

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        // $userInfo = User::find(Auth::id())->first();
        // dd($userInfo);
        return view('office_supplies_manage.index', [
            'office_supplies_manage' => ManageItems::query()
            ->where('inventory_stock_no', 'like', "%{$request->search}%")->where('type','office-supplies')->paginate(15), 
            // 'userInfo'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $laststockno = InventoryStockNoCounter::latest('created_at')->limit(1)->first();
        $stockno = intval($laststockno->last_no) + 1;
        return view('office_supplies_manage.create', compact('stockno'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ManageItems $office_supplies_manage)
    {
        $data = [
            'last_no' => $request['inventory_stock_no']
        ];
        InventoryStockNoCounter::create($data);
        $request['type'] = "office-supplies";
        $office_supplies_manage->create($request->all());

        return redirect()
            ->route('office_supplies_manage.index')
            ->withStatus('Office Supplies successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ManageItems $office_supplies_manage)
    {
        return view('office_supplies_manage.show', [
            'office_supplies_manage' => $office_supplies_manage
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ManageItems $office_supplies_manage)
    {
        return view('office_supplies_manage.edit', compact('office_supplies_manage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ManageItems $office_supplies_manage)
    {
        $office_supplies_manage->update($request->all());

        return redirect()
            ->route('office_supplies_manage.index')
            ->withStatus('Office Supplies updated satisfactorily.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ManageItems $office_supplies_manage)
    {
        $office_supplies_manage->delete();
        
        return back()->withStatus('Office Supplies successfully removed.');
    }
}
