<?php

namespace App\Http\Controllers;
use App\StockInItems;
use App\ManageItems;
use Illuminate\Http\Request;

class BooksInController extends Controller
{
    //
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $books_in = StockInItems::query()
        ->whereHas('ManageItems', function($q) use($request){
            $q->where('inventory_stock_no', 'like', "%{$request->search}%");
        })->with('ManageItems')->where('type','books')->paginate(15);

        return view('books_in.index', compact('books_in'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $books = ManageItems::where('type','books')->get(['id', 'inventory_stock_no']);
        return view('books_in.create', compact('books'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, StockInItems $books_in)
    {
        $request['type'] = "books";
        $books_in->create($request->all());
        $books = ManageItems::where('type','books')->where('id',$request->inventory_stock_no_id)->first();
        $books->stock_on_hand = intval($books->stock_on_hand) + intval($request->quantity);
        $books->date_in = $request->date_in;
        $books->save();
        return redirect()
            ->route('books_in.index')
            ->withStatus('Stock-in successfully.');
    }
}
