<?php

namespace App\Http\Controllers;
use App\StockInItems;
use App\ManageItems;
use Illuminate\Http\Request;

class ToolsInController extends Controller
{
    //
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $tools_in = StockInItems::query()
        ->whereHas('ManageItems', function($q) use($request){
            $q->where('inventory_stock_no', 'like', "%{$request->search}%");
        })->with('ManageItems')->where('type','tools')->paginate(15);

        return view('tools_in.index', compact('tools_in'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tools = ManageItems::where('type','tools')->get(['id', 'inventory_stock_no']);
        return view('tools_in.create', compact('tools'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, StockInItems $tools_in)
    {
        $request['type'] = "tools";
        $tools_in->create($request->all());
        $tools = ManageItems::where('type','tools')->where('id',$request->inventory_stock_no_id)->first();
        $tools->stock_on_hand = intval($tools->stock_on_hand) + intval($request->quantity);
        $tools->date_in = $request->date_in;
        $tools->save();
        return redirect()
            ->route('tools_in.index')
            ->withStatus('Stock-in successfully.');
    }
}
