<?php

namespace App\Http\Controllers;
use App\NewInventory;
use Illuminate\Http\Request;

class NewInventoryController extends Controller
{
    //
    //
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('new_inventory.index', [
            'new_inventory' => NewInventory::query()
            ->where('item', 'like', "%{$request->search}%")
            ->orWhere('property_no', 'like', "%{$request->search}%")->paginate(15), 
            // 'month' => Carbon::now()->month
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('new_inventory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, NewInventory $new_inventory)
    {
        $new_inventory->create($request->all());

        return redirect()
            ->route('new_inventory.index')
            ->withStatus('New inventory checklist successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(NewInventory $new_inventory)
    {
        return view('new_inventory.show', [
            'new_inventory' => $new_inventory
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(NewInventory $new_inventory)
    {
        return view('new_inventory.edit', compact('new_inventory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NewInventory $new_inventory)
    {
        $new_inventory->update($request->all());

        return redirect()
            ->route('new_inventory.index')
            ->withStatus('New inventory checklist updated satisfactorily.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(NewInventory $new_inventory)
    {
        $new_inventory->delete();
        
        return back()->withStatus('New inventory checklist successfully removed.');
    }
}
