<?php

namespace App\Http\Controllers;
use App\StockOutItems;
use App\ManageItems;
use Illuminate\Http\Request;

class IctEquipOutController extends Controller
{
    //
    //
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $ict_equips_out = StockOutItems::query()
        ->whereHas('ManageItems', function($q) use($request){
            $q->where('inventory_stock_no', 'like', "%{$request->search}%");
        })->with('ManageItems')->where('type','ict-equipment')->paginate(15);

        return view('ict_equips_out.index', compact('ict_equips_out'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ict_equips = ManageItems::where('type','ict-equipment')->get(['id', 'inventory_stock_no']);
        return view('ict_equips_out.create', compact('ict_equips'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, StockOutItems $ict_equips_out)
    {
        $request['type'] = "ict-equipment";
        $ict_equips_out->create($request->all());
        $ict_equips = ManageItems::where('type','ict-equipment')->where('id',$request->inventory_stock_no_id)->first();
        $ict_equips->stock_on_hand = intval($ict_equips->stock_on_hand) - intval($request->quantity);
        $ict_equips->date_out = $request->date_out;
        $ict_equips->save();
        return redirect()
            ->route('ict_equips_out.index')
            ->withStatus('Stock-out successfully.');
    }
}
