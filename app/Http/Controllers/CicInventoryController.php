<?php

namespace App\Http\Controllers;
use App\CicInventory;
use Illuminate\Http\Request;

class CicInventoryController extends Controller
{
    //
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('cic_inventory.index', [
            'cic_inventory' => CicInventory::query()
            ->where('item', 'like', "%{$request->search}%")
            ->orWhere('property_no', 'like', "%{$request->search}%")->paginate(15), 
            // 'month' => Carbon::now()->month
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cic_inventory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, CicInventory $cic_inventory)
    {
        $cic_inventory->create($request->all());

        return redirect()
            ->route('cic_inventory.index')
            ->withStatus('CIC inventory checklist successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(CicInventory $cic_inventory)
    {
        return view('cic_inventory.show', [
            'cic_inventory' => $cic_inventory
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CicInventory $cic_inventory)
    {
        return view('cic_inventory.edit', compact('cic_inventory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CicInventory $cic_inventory)
    {
        $cic_inventory->update($request->all());

        return redirect()
            ->route('cic_inventory.index')
            ->withStatus('CIC inventory checklist updated satisfactorily.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CicInventory $cic_inventory)
    {
        $cic_inventory->delete();
        
        return back()->withStatus('CIC inventory checklist successfully removed.');
    }
}
