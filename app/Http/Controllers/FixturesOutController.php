<?php

namespace App\Http\Controllers;
use App\StockOutItems;
use App\ManageItems;
use Illuminate\Http\Request;

class FixturesOutController extends Controller
{
    //
    //
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $fixtures_out = StockOutItems::query()
        ->whereHas('ManageItems', function($q) use($request){
            $q->where('inventory_stock_no', 'like', "%{$request->search}%");
        })->with('ManageItems')->where('type','fixtures')->paginate(15);

        return view('fixtures_out.index', compact('fixtures_out'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fixtures = ManageItems::where('type','fixtures')->get(['id', 'inventory_stock_no']);
        return view('fixtures_out.create', compact('fixtures'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, StockOutItems $fixtures_out)
    {
        $request['type'] = "fixtures";
        $fixtures_out->create($request->all());
        $fixtures = ManageItems::where('type','fixtures')->where('id',$request->inventory_stock_no_id)->first();
        $fixtures->stock_on_hand = intval($fixtures->stock_on_hand) - intval($request->quantity);
        $fixtures->date_out = $request->date_out;
        $fixtures->save();
        return redirect()
            ->route('fixtures_out.index')
            ->withStatus('Stock-out successfully.');
    }
}
