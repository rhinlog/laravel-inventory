<?php

namespace App\Http\Controllers;
use App\StockOutItems;
use App\ManageItems;
use Illuminate\Http\Request;

class BooksOutController extends Controller
{
    //
    //
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $books_out = StockOutItems::query()
        ->whereHas('ManageItems', function($q) use($request){
            $q->where('inventory_stock_no', 'like', "%{$request->search}%");
        })->with('ManageItems')->where('type','books')->paginate(15);

        return view('books_out.index', compact('books_out'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $books = ManageItems::where('type','books')->get(['id', 'inventory_stock_no']);
        return view('books_out.create', compact('books'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, StockOutItems $books_out)
    {
        $request['type'] = "books";
        $books_out->create($request->all());
        $books = ManageItems::where('type','books')->where('id',$request->inventory_stock_no_id)->first();
        $books->stock_on_hand = intval($books->stock_on_hand) - intval($request->quantity);
        $books->date_out = $request->date_out;
        $books->save();
        return redirect()
            ->route('books_out.index')
            ->withStatus('Stock-out successfully.');
    }
}
