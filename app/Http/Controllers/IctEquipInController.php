<?php

namespace App\Http\Controllers;
use App\StockInItems;
use App\ManageItems;
use Illuminate\Http\Request;

class IctEquipInController extends Controller
{
    //
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $ict_equips_in = StockInItems::query()
        ->whereHas('ManageItems', function($q) use($request){
            $q->where('inventory_stock_no', 'like', "%{$request->search}%");
        })->with('ManageItems')->where('type','ict-equipment')->paginate(15);

        return view('ict_equips_in.index', compact('ict_equips_in'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ict_equips = ManageItems::where('type','ict-equipment')->get(['id', 'inventory_stock_no']);
        return view('ict_equips_in.create', compact('ict_equips'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, StockInItems $ict_equips_in)
    {
        $request['type'] = "ict-equipment";
        $ict_equips_in->create($request->all());
        $ict_equips = ManageItems::where('type','ict-equipment')->where('id',$request->inventory_stock_no_id)->first();
        $ict_equips->stock_on_hand = intval($ict_equips->stock_on_hand) + intval($request->quantity);
        $ict_equips->date_in = $request->date_in;
        $ict_equips->save();
        return redirect()
            ->route('ict_equips_in.index')
            ->withStatus('Stock-in successfully.');
    }
}
