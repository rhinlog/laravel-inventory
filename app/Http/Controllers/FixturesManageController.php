<?php

namespace App\Http\Controllers;
use App\ManageItems;
use App\InventoryStockNoCounter;
use App\User;
use Auth;
use Illuminate\Http\Request;

class FixturesManageController extends Controller
{
    //

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        // $userInfo = User::find(Auth::id())->first();
        // dd($userInfo);
        return view('fixtures_manage.index', [
            'fixtures_manage' => ManageItems::query()
            ->where('inventory_stock_no', 'like', "%{$request->search}%")->where('type','fixtures')->paginate(15), 
            // 'userInfo'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $laststockno = InventoryStockNoCounter::latest('created_at')->limit(1)->first();
        $stockno = intval($laststockno->last_no) + 1;
        return view('fixtures_manage.create', compact('stockno'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ManageItems $fixtures_manage)
    {
        $data = [
            'last_no' => $request['inventory_stock_no']
        ];
        InventoryStockNoCounter::create($data);
        $request['type'] = "fixtures";
        $fixtures_manage->create($request->all());

        return redirect()
            ->route('fixtures_manage.index')
            ->withStatus('Fixtures successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ManageItems $fixtures_manage)
    {
        return view('fixtures_manage.show', [
            'fixtures_manage' => $fixtures_manage
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ManageItems $fixtures_manage)
    {
        return view('fixtures_manage.edit', compact('fixtures_manage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ManageItems $fixtures_manage)
    {
        $fixtures_manage->update($request->all());

        return redirect()
            ->route('fixtures_manage.index')
            ->withStatus('Fixtures updated satisfactorily.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ManageItems $fixtures_manage)
    {
        $fixtures_manage->delete();
        
        return back()->withStatus('Fixtures successfully removed.');
    }
}
