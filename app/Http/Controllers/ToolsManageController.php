<?php

namespace App\Http\Controllers;
use App\ManageItems;
use App\InventoryStockNoCounter;
use App\User;
use Auth;
use Illuminate\Http\Request;

class ToolsManageController extends Controller
{
    //

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        // $userInfo = User::find(Auth::id())->first();
        // dd($userInfo);
        return view('tools_manage.index', [
            'tools_manage' => ManageItems::query()
            ->where('inventory_stock_no', 'like', "%{$request->search}%")->where('type','tools')->paginate(15), 
            // 'userInfo'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $laststockno = InventoryStockNoCounter::latest('created_at')->limit(1)->first();
        $stockno = intval($laststockno->last_no) + 1;
        return view('tools_manage.create', compact('stockno'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ManageItems $tools_manage)
    {
        $data = [
            'last_no' => $request['inventory_stock_no']
        ];
        InventoryStockNoCounter::create($data);
        $request['type'] = "tools";
        $tools_manage->create($request->all());

        return redirect()
            ->route('tools_manage.index')
            ->withStatus('Tools successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ManageItems $tools_manage)
    {
        return view('tools_manage.show', [
            'tools_manage' => $tools_manage
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ManageItems $tools_manage)
    {
        return view('tools_manage.edit', compact('tools_manage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ManageItems $tools_manage)
    {
        $tools_manage->update($request->all());

        return redirect()
            ->route('tools_manage.index')
            ->withStatus('Tools updated satisfactorily.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ManageItems $tools_manage)
    {
        $tools_manage->delete();
        
        return back()->withStatus('Tools successfully removed.');
    }
}
