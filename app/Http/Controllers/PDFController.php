<?php

namespace App\Http\Controllers;
use PDF;
use App\CicInventory;
use App\NewInventory;
use App\ManageItems;
use Illuminate\Http\Request;

class PDFController extends Controller
{
    //

    public function cic_inventory()
    {	
    	$data = CicInventory::get();
    	$pdf = PDF::loadView('pdfs.cic_inventory', compact('data'),[], 
        [ 
          'orientation' => 'L'
        ]);
		return $pdf->stream('CIC Inventory Checklist.pdf');
    }

    public function new_inventory()
    {	
    	$data = NewInventory::get();
    	$pdf = PDF::loadView('pdfs.new_inventory', compact('data'),[], 
        [ 
          'orientation' => 'L'
        ]);
		return $pdf->stream('New Inventory Checklist.pdf');
    }

    public function ict_equips_manage()
    {	
    	$data = ManageItems::where('type','ict-equipment')->get();
    	$pdf = PDF::loadView('pdfs.ict_equips_manage', compact('data'),[], 
        [ 
          'orientation' => 'L'
        ]);
		return $pdf->stream('ICT Equipments.pdf');
    }

    public function furnitures_manage()
    {	
    	$data = ManageItems::where('type','furnitures')->get();
    	$pdf = PDF::loadView('pdfs.furnitures_manage', compact('data'),[], 
        [ 
          'orientation' => 'L'
        ]);
		return $pdf->stream('Furnitures.pdf');
    }

    public function fixtures_manage()
    {	
    	$data = ManageItems::where('type','fixtures')->get();
    	$pdf = PDF::loadView('pdfs.fixtures_manage', compact('data'),[], 
        [ 
          'orientation' => 'L'
        ]);
		return $pdf->stream('Fixtures.pdf');
    }

    public function foods_manage()
    {	
    	$data = ManageItems::where('type','foods')->get();
    	$pdf = PDF::loadView('pdfs.foods_manage', compact('data'),[], 
        [ 
          'orientation' => 'L'
        ]);
		return $pdf->stream('Foods.pdf');
    }

    public function books_manage()
    {	
    	$data = ManageItems::where('type','books')->get();
    	$pdf = PDF::loadView('pdfs.books_manage', compact('data'),[], 
        [ 
          'orientation' => 'L'
        ]);
		return $pdf->stream('Books.pdf');
    }

    public function tools_manage()
    {	
    	$data = ManageItems::where('type','tools')->get();
    	$pdf = PDF::loadView('pdfs.tools_manage', compact('data'),[], 
        [ 
          'orientation' => 'L'
        ]);
		return $pdf->stream('Tools.pdf');
    }

    public function office_supplies_manage()
    {	
    	$data = ManageItems::where('type','office-supplies')->get();
    	$pdf = PDF::loadView('pdfs.office_supplies_manage', compact('data'),[], 
        [ 
          'orientation' => 'L'
        ]);
		return $pdf->stream('Office Supplies.pdf');
    }
}
