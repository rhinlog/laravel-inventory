<?php

namespace App\Http\Controllers;
use App\FileUpload;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;

class FilesController extends Controller
{
    //
    public function index()
    {
        $files = FileUpload::orderBy('id','DESC')->paginate(15);
        return view('files.index', compact('files'));
    }

    public function create()
    {
        return view('files.create');
    }

    public function store(Request $request)
    {
        $files = $request->all();
        $files['uuid'] = (string)Uuid::generate();
        if ($request->hasFile('file')) {
            $files['file'] = $request->file->getClientOriginalName();
            $request->file->storeAs('uploads/', $files['file']);
        }
        FileUpload::create($files);
        return redirect()->route('files.index')->withStatus('Uploaded successfully.');
    }

    public function download($uuid)
    {
        $files = FileUpload::where('uuid', $uuid)->firstOrFail();
        $pathToFile = storage_path('app/uploads/'."\\".$files->file);
        return response()->download($pathToFile);
    }
}
