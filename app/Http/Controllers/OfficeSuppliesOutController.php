<?php

namespace App\Http\Controllers;
use App\StockOutItems;
use App\ManageItems;
use Illuminate\Http\Request;

class OfficeSuppliesOutController extends Controller
{
    //
    //
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $office_supplies_out = StockOutItems::query()
        ->whereHas('ManageItems', function($q) use($request){
            $q->where('inventory_stock_no', 'like', "%{$request->search}%");
        })->with('ManageItems')->where('type','office-supplies')->paginate(15);

        return view('office_supplies_out.index', compact('office_supplies_out'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $office_supplies = ManageItems::where('type','office-supplies')->get(['id', 'inventory_stock_no']);
        return view('office_supplies_out.create', compact('office_supplies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, StockOutItems $office_supplies_out)
    {
        $request['type'] = "office-supplies";
        $office_supplies_out->create($request->all());
        $office_supplies = ManageItems::where('type','office-supplies')->where('id',$request->inventory_stock_no_id)->first();
        $office_supplies->stock_on_hand = intval($office_supplies->stock_on_hand) - intval($request->quantity);
        $office_supplies->date_out = $request->date_out;
        $office_supplies->save();
        return redirect()
            ->route('office_supplies_out.index')
            ->withStatus('Stock-out successfully.');
    }
}
