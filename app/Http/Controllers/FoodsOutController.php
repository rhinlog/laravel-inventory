<?php

namespace App\Http\Controllers;
use App\StockOutItems;
use App\ManageItems;
use Illuminate\Http\Request;

class FoodsOutController extends Controller
{
    //
    //
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $foods_out = StockOutItems::query()
        ->whereHas('ManageItems', function($q) use($request){
            $q->where('inventory_stock_no', 'like', "%{$request->search}%");
        })->with('ManageItems')->where('type','foods')->paginate(15);

        return view('foods_out.index', compact('foods_out'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $foods = ManageItems::where('type','foods')->get(['id', 'inventory_stock_no']);
        return view('foods_out.create', compact('foods'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, StockOutItems $foods_out)
    {
        $request['type'] = "foods";
        $foods_out->create($request->all());
        $foods = ManageItems::where('type','foods')->where('id',$request->inventory_stock_no_id)->first();
        $foods->stock_on_hand = intval($foods->stock_on_hand) - intval($request->quantity);
        $foods->date_out = $request->date_out;
        $foods->save();
        return redirect()
            ->route('foods_out.index')
            ->withStatus('Stock-out successfully.');
    }
}
