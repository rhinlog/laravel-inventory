<?php

namespace App\Http\Controllers;
use App\StockInItems;
use App\ManageItems;
use Illuminate\Http\Request;

class FixturesInController extends Controller
{
    //
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $fixtures_in = StockInItems::query()
        ->whereHas('ManageItems', function($q) use($request){
            $q->where('inventory_stock_no', 'like', "%{$request->search}%");
        })->with('ManageItems')->where('type','fixtures')->paginate(15);

        return view('fixtures_in.index', compact('fixtures_in'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fixtures = ManageItems::where('type','fixtures')->get(['id', 'inventory_stock_no']);
        return view('fixtures_in.create', compact('fixtures'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, StockInItems $fixtures_in)
    {
        $request['type'] = "fixtures";
        $fixtures_in->create($request->all());
        $fixtures = ManageItems::where('type','fixtures')->where('id',$request->inventory_stock_no_id)->first();
        $fixtures->stock_on_hand = intval($fixtures->stock_on_hand) + intval($request->quantity);
        $fixtures->date_in = $request->date_in;
        $fixtures->save();
        return redirect()
            ->route('fixtures_in.index')
            ->withStatus('Stock-in successfully.');
    }
}
