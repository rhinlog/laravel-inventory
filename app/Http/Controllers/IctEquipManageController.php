<?php

namespace App\Http\Controllers;
use App\ManageItems;
use App\InventoryStockNoCounter;
use App\User;
use Auth;
use Illuminate\Http\Request;

class IctEquipManageController extends Controller
{
    //

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        // $userInfo = User::find(Auth::id())->first();
        // dd($userInfo);
        return view('ict_equips_manage.index', [
            'ict_equips_manage' => ManageItems::query()
            ->where('inventory_stock_no', 'like', "%{$request->search}%")->where('type','ict-equipment')->paginate(15), 
            // 'userInfo'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $laststockno = InventoryStockNoCounter::latest('created_at')->limit(1)->first();
        $stockno = intval($laststockno->last_no) + 1;
        return view('ict_equips_manage.create', compact('stockno'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ManageItems $ict_equips_manage)
    {
        $data = [
            'last_no' => $request['inventory_stock_no']
        ];
        InventoryStockNoCounter::create($data);
        $request['type'] = "ict-equipment";
        $ict_equips_manage->create($request->all());

        return redirect()
            ->route('ict_equips_manage.index')
            ->withStatus('Ict equipment successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ManageItems $ict_equips_manage)
    {
        return view('ict_equips_manage.show', [
            'ict_equips_manage' => $ict_equips_manage
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ManageItems $ict_equips_manage)
    {
        return view('ict_equips_manage.edit', compact('ict_equips_manage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ManageItems $ict_equips_manage)
    {
        $ict_equips_manage->update($request->all());

        return redirect()
            ->route('ict_equips_manage.index')
            ->withStatus('Ict equipment updated satisfactorily.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ManageItems $ict_equips_manage)
    {
        $ict_equips_manage->delete();
        
        return back()->withStatus('Ict equipment successfully removed.');
    }
}
