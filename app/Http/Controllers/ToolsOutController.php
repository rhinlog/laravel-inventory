<?php

namespace App\Http\Controllers;
use App\StockOutItems;
use App\ManageItems;
use Illuminate\Http\Request;

class ToolsOutController extends Controller
{
    //
    //
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $tools_out = StockOutItems::query()
        ->whereHas('ManageItems', function($q) use($request){
            $q->where('inventory_stock_no', 'like', "%{$request->search}%");
        })->with('ManageItems')->where('type','tools')->paginate(15);

        return view('tools_out.index', compact('tools_out'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tools = ManageItems::where('type','tools')->get(['id', 'inventory_stock_no']);
        return view('tools_out.create', compact('tools'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, StockOutItems $tools_out)
    {
        $request['type'] = "tools";
        $tools_out->create($request->all());
        $tools = ManageItems::where('type','tools')->where('id',$request->inventory_stock_no_id)->first();
        $tools->stock_on_hand = intval($tools->stock_on_hand) - intval($request->quantity);
        $tools->date_out = $request->date_out;
        $tools->save();
        return redirect()
            ->route('tools_out.index')
            ->withStatus('Stock-out successfully.');
    }
}
