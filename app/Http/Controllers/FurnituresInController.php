<?php

namespace App\Http\Controllers;
use App\StockInItems;
use App\ManageItems;
use Illuminate\Http\Request;

class FurnituresInController extends Controller
{
    //
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $furnitures_in = StockInItems::query()
        ->whereHas('ManageItems', function($q) use($request){
            $q->where('inventory_stock_no', 'like', "%{$request->search}%");
        })->with('ManageItems')->where('type','furnitures')->paginate(15);

        return view('furnitures_in.index', compact('furnitures_in'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $furnitures = ManageItems::where('type','furnitures')->get(['id', 'inventory_stock_no']);
        return view('furnitures_in.create', compact('furnitures'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, StockInItems $furnitures_in)
    {
        $request['type'] = "furnitures";
        $furnitures_in->create($request->all());
        $furnitures = ManageItems::where('type','furnitures')->where('id',$request->inventory_stock_no_id)->first();
        $furnitures->stock_on_hand = intval($furnitures->stock_on_hand) + intval($request->quantity);
        $furnitures->date_in = $request->date_in;
        $furnitures->save();
        return redirect()
            ->route('furnitures_in.index')
            ->withStatus('Stock-in successfully.');
    }
}
