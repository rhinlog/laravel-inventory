<?php

namespace App\Http\Controllers;
use App\StockOutItems;
use App\ManageItems;
use Illuminate\Http\Request;

class FurnituresOutController extends Controller
{
    //
    //
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $furnitures_out = StockOutItems::query()
        ->whereHas('ManageItems', function($q) use($request){
            $q->where('inventory_stock_no', 'like', "%{$request->search}%");
        })->with('ManageItems')->where('type','furnitures')->paginate(15);

        return view('furnitures_out.index', compact('furnitures_out'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $furnitures = ManageItems::where('type','furnitures')->get(['id', 'inventory_stock_no']);
        return view('furnitures_out.create', compact('furnitures'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, StockOutItems $furnitures_out)
    {
        $request['type'] = "furnitures";
        $furnitures_out->create($request->all());
        $furnitures = ManageItems::where('type','furnitures')->where('id',$request->inventory_stock_no_id)->first();
        $furnitures->stock_on_hand = intval($furnitures->stock_on_hand) - intval($request->quantity);
        $furnitures->date_out = $request->date_out;
        $furnitures->save();
        return redirect()
            ->route('furnitures_out.index')
            ->withStatus('Stock-out successfully.');
    }
}
