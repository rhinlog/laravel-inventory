<?php

namespace App\Http\Controllers;
use App\ManageItems;
use App\InventoryStockNoCounter;
use App\User;
use Auth;
use Illuminate\Http\Request;

class BooksManageController extends Controller
{
    //

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        // $userInfo = User::find(Auth::id())->first();
        // dd($userInfo);
        return view('books_manage.index', [
            'books_manage' => ManageItems::query()
            ->where('inventory_stock_no', 'like', "%{$request->search}%")->where('type','books')->paginate(15), 
            // 'userInfo'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $laststockno = InventoryStockNoCounter::latest('created_at')->limit(1)->first();
        $stockno = intval($laststockno->last_no) + 1;
        return view('books_manage.create', compact('stockno'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ManageItems $books_manage)
    {
        $data = [
            'last_no' => $request['inventory_stock_no']
        ];
        InventoryStockNoCounter::create($data);
        $request['type'] = "books";
        $books_manage->create($request->all());
        return redirect()
            ->route('books_manage.index')
            ->withStatus('Books successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ManageItems $books_manage)
    {
        return view('books_manage.show', [
            'books_manage' => $books_manage
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ManageItems $books_manage)
    {
        return view('books_manage.edit', compact('books_manage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ManageItems $books_manage)
    {
        $books_manage->update($request->all());

        return redirect()
            ->route('books_manage.index')
            ->withStatus('Books updated satisfactorily.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ManageItems $books_manage)
    {
        $books_manage->delete();
        
        return back()->withStatus('Books successfully removed.');
    }
}
