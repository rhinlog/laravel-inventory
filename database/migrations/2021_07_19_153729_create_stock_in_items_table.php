<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockInItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_in_items', function (Blueprint $table) {
            $table->id();
            $table->integer('inventory_stock_no_id');
            $table->string('description');
            $table->string('unit');
            $table->string('quantity');
            $table->string('date_in');
            $table->string('received_by');
            $table->string('transferred_to');
            $table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_in_items');
    }
}
