<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManageItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manage_items', function (Blueprint $table) {
            $table->id();
            $table->string('inventory_stock_no')->unique();
            $table->string('description');
            $table->string('unit');
            // $table->string('quantity');
            $table->string('date_in')->nullable();
            $table->string('stock_on_hand');
            $table->string('date_out')->nullable();
            $table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manage_items');
    }
}
