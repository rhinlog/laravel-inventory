<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    protected $toTruncate=[
        'users',
    ];
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        foreach($this->toTruncate as $table) {
            \Illuminate\Support\Facades\DB::table($table)->truncate();
        }

        $this->call([
            CounterSeeder::class,
            UserSeeder::class
            
        ]);

        Model::reguard();
    }
}
