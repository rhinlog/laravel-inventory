<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CounterSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $date = date('Y-m-d H:i:s');
        $data = [
            [   
                'last_no' => '100000',
                'created_at' => $date,
                'updated_at' => $date,
            ],
        ];

        \App\InventoryStockNoCounter::insert($data);
    }
}
