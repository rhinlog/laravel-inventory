@extends('layouts.app', ['page' => 'Furnitures', 'pageSlug' => 'furnitures_manage-create', 'section' => 'transactions'])

@section('content')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Furnitures</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('furnitures_manage.index') }}" class="btn btn-sm btn-primary">Back to List</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('furnitures_manage.store', $stockno) }}" autocomplete="off">
                            @csrf

                            <h6 class="heading-small text-muted mb-4">Furnitures Information</h6>
                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('inventory_stock_no') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-inventory_stock_no">Inventory Stock No.</label>
                                    <input type="number" name="inventory_stock_no" id="input-inventory_stock_no" class="form-control form-control-alternative{{ $errors->has('inventory_stock_no') ? ' is-invalid' : '' }}" value="{{ old('inventory_stock_no', $stockno) }}" readonly required autofocus>
                                    @include('alerts.feedback', ['field' => 'item'])
                                </div>
                                <div class="form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-description">Description</label>
                                    <input type="text" name="description" id="input-description" class="form-control form-control-alternative{{ $errors->has('description') ? ' is-invalid' : '' }}"value="{{ old('description') }}" required autofocus>
                                    @include('alerts.feedback', ['field' => 'description'])
                                </div>
                                <div class="form-group{{ $errors->has('unit') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">Unit</label>
                                    <input type="number" name="unit" id="input-unit" class="form-control form-control-alternative{{ $errors->has('unit') ? ' is-invalid' : '' }}"value="{{ old('unit') }}" required autofocus>
                                    @include('alerts.feedback', ['field' => 'unit'])
                                </div>
                                <!-- <div class="form-group{{ $errors->has('quantity') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-quantity">Quantity</label>
                                    <input type="number" name="quantity" id="input-quantity" class="form-control form-control-alternative{{ $errors->has('quantity') ? ' is-invalid' : '' }}"value="{{ old('quantity') }}" required autofocus>
                                    @include('alerts.feedback', ['field' => 'quantity'])
                                </div> -->
                                <div class="form-group{{ $errors->has('stock_on_hand') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-stock_on_hand">Stock On Hand</label>
                                    <input type="number" name="stock_on_hand" id="input-stock_on_hand" class="form-control form-control-alternative{{ $errors->has('stock_on_hand') ? ' is-invalid' : '' }}" value="{{ old('stock_on_hand') }}" required>
                                    @include('alerts.feedback', ['field' => 'stock_on_hand'])
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
