@extends('layouts.app', ['page' => 'Files', 'pageSlug' => 'files-create', 'section' => 'transactions'])

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h4 class="mb-0">Add File</h4>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('files.index') }}" class="btn btn-sm btn-primary">Back to List</a>
                            </div>
                        </div>
                    </div>
                <div class="card-body">

                    <form action="{{ route('files.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <h6 class="heading-small text-muted mb-4">File Information</h6>
                        <div class="pl-lg-4">
                            <label class="form-control-label" for="input-file_name">File Name:</label>
                            <br>
                            <input type="text" name="file_name" class="form-control">
                            <br>
                            <label class="form-control-label" for="input-file_name">File: (Must be a compressed file such as: zip,rar or 7zip)</label>
                            <br>
                            <input type="file" name="file" id="file" accept=".zip,.rar,.7zip">
                            <br><br>
                            <input type="submit" value="Upload File" class="btn btn-sm btn-primary">
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection 