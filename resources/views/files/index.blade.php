@extends('layouts.app', ['page' => 'Files', 'pageSlug' => 'files', 'section' => 'files'])

@section('content')
@include('alerts.success')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-8">
                            <h4 class="mb-0">File Lists</h4>
                        </div>
                        @if(Auth::user()->role == 'admin')
                        <div class="col-4 text-right">
                            <a href="{{ route('files.create') }}" class="btn btn-sm btn-primary">Upload</a>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    <div class="">
                        <table class="table tablesorter " id="">
                            <thead class=" text-primary">
                                <th>File Name</th>
                                <th>File (Click link to download)</th>
                                <th>Date Uploaded</th>
                            </thead>
                            <tbody>
                            @forelse ($files as $file)
                                <tr>
                                    <td>{{ $file->file_name }}</td>
                                    <td><a href="{{ route('files.download', $file->uuid) }}">{{ $file->file }}</a></td>
                                    <td>{{ date('Y-m-d', strtotime($file->created_at)) }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="2">No Excel File found.</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">
                       {{ $files->links() }}
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection 