@extends('layouts.app', ['page' => 'Foods Stock-in', 'pageSlug' => 'foods_in', 'section' => 'foods_in'])

@section('content')
    @include('alerts.success')
    <div class="row">
        <div class="col-md-12">
            <div class="card ">
                <div class="card-header">
                    <div class="row">
                        <div class="col-8">
                            <h4 class="card-title"><strong>Foods Stock-in</strong></h4>
                            <form action="{{ route('foods_in.index') }}" method="GET">
                                <input class="pl-lg-4" type="text" name="search" placeholder="Search Stock No." required/>
                                <button type="submit" class="btn btn-sm btn-secondary">Search</button>
                                <a href="{{ route('foods_in.index') }}" class="btn btn-sm btn-secondary">Refresh</a>
                            </form>
                        </div>
                        <div class="col-4 text-right">
                        @if(Auth::user()->role == 'admin')
                            <a href="{{ route('foods_in.create') }}" class="btn btn-sm btn-primary">Stock-in</a>
                            <!-- <a href="{{ 'api/books_in/pdf' }}" class="btn btn-sm btn-primary">Print</a> -->
                        @endif
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    
                    <div class="">
                        <table class="table tablesorter " id="">
                            <thead class=" text-primary">
                                <th scope="col">Inventory Stock No.</th>
                                <th scope="col">Description</th>
                                <th scope="col">Unit</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Date In</th>
                                <th scope="col">Received By</th>
                                <th scope="col">Transferred To</th>
                            </thead>
                            <tbody>
                                @foreach ($foods_in as $item)
                                    <tr>
                                        <td>{{ $item->ManageItems->inventory_stock_no??'' }}</td>
                                        <td>{{ $item->description??'' }}</td>
                                        <td>{{ $item->unit??'' }}</td>
                                        <td>{{ $item->quantity??'' }}</td>
                                        <td>{{ $item->date_in??'' }}</td>
                                        <td>{{ $item->received_by??'' }}</td>
                                        <td>{{ $item->transferred_to??'' }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
               </div>
                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">
                       {{ $foods_in->links() }}
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection
