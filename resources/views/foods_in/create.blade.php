@extends('layouts.app', ['page' => 'Foods Stock-in', 'pageSlug' => 'foods_in-create', 'section' => 'transactions'])

@section('content')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Foods Stock-in</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('foods_in.index') }}" class="btn btn-sm btn-primary">Back to List</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('foods_in.store') }}" autocomplete="off">
                            @csrf
                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('inventory_stock_no_id') ? ' has-danger' : '' }}">
                                <select style="color:gray;" class="form-control" name="inventory_stock_no_id">
                                <option selected="true" disabled="disabled">Select Inventory Stock No.</option>  
                                    @foreach($foods as $item)
                                    <option value="{{$item->id}}">{{$item->inventory_stock_no}}</option>
                                    @endforeach
                                </select>
                                    @include('alerts.feedback', ['field' => 'inventory_stock_no_id'])
                                </div>
                                <div class="form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-description">Description</label>
                                    <input type="text" name="description" id="input-description" class="form-control form-control-alternative{{ $errors->has('description') ? ' is-invalid' : '' }}"value="{{ old('description') }}" required autofocus>
                                    @include('alerts.feedback', ['field' => 'description'])
                                </div>
                                <div class="form-group{{ $errors->has('unit') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">Unit</label>
                                    <input type="number" name="unit" id="input-unit" class="form-control form-control-alternative{{ $errors->has('unit') ? ' is-invalid' : '' }}"value="{{ old('unit') }}" required autofocus>
                                    @include('alerts.feedback', ['field' => 'unit'])
                                </div>
                                <div class="form-group{{ $errors->has('quantity') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-quantity">Quantity</label>
                                    <input type="number" name="quantity" id="input-quantity" class="form-control form-control-alternative{{ $errors->has('quantity') ? ' is-invalid' : '' }}"value="{{ old('quantity') }}" required autofocus>
                                    @include('alerts.feedback', ['field' => 'quantity'])
                                </div>
                                <div class="form-group{{ $errors->has('date_in') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-date_in">Date In</label>
                                    <input type="date" name="date_in" id="input-date_in" class="form-control form-control-alternative{{ $errors->has('date_in') ? ' is-invalid' : '' }}"value="{{ old('date_in') }}" required autofocus>
                                    @include('alerts.feedback', ['field' => 'date_in'])
                                </div>
                                <div class="form-group{{ $errors->has('received_by') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-received_by">Received By</label>
                                    <input type="text" name="received_by" id="input-received_by" class="form-control form-control-alternative{{ $errors->has('received_by') ? ' is-invalid' : '' }}" value="{{ old('received_by') }}" required>
                                    @include('alerts.feedback', ['field' => 'received_by'])
                                </div>
                                <div class="form-group{{ $errors->has('transferred_to') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-transferred_to">Transferre To</label>
                                    <input type="text" name="transferred_to" id="input-transferred_to" class="form-control form-control-alternative{{ $errors->has('transferred_to') ? ' is-invalid' : '' }}" value="{{ old('transferred_to') }}" required>
                                    @include('alerts.feedback', ['field' => 'transferred_to'])
                                </div>
                                
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
