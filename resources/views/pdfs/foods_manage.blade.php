<!DOCTYPE html>
<html>
<head>
<style>


table {
 font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  margin-bottom: 50px;
  margin-top: 50px;

}

caption {
  text-align: left;
  color: silver;
  font-weight: bold;
  text-transform: uppercase;
  padding: 5px;
}

thead {
    background: SteelBlue;
    border: 2px solid #000000;
  color: white;
}

th{
    background: SteelBlue;
    border: 2px solid #000000;
  color: white;
}
td {
  border: 2px solid #000000;
  padding: 2px;
}
.tables1{
  width: 100%;
}
.thyellow{
  background-color: #f4df0b;
}
.thblue{
  background-color: #2495db;
}

</style>
</head>
    <body>
    <h1 style="text-align:center;">Foods</h1>
    <table class="tables1">
        <tr>
        <th scope="col">Inventory Stock No.</th>
        <th scope="col">Description</th>
        <th scope="col">Unit</th>
        <th scope="col">Date In</th>
        <th scope="col">Stock On Hand</th>
        <th scope="col">Date Out</th>
        </tr>
        @foreach($data as $key => $item)
          <tr>
          <td>{{ $item->inventory_stock_no }}</td>
          <td>{{ $item->description }}</td>
          <td>{{ $item->unit }}</td>
          <td>{{ $item->date_in }}</td>
          <td>{{ $item->stock_on_hand }}</td>
          <td>{{ $item->date_out }}</td>
          </tr>
        @endforeach
    </table>
  </body>
</html>