@extends('layouts.app', ['page' => 'Furnitures Request', 'pageSlug' => 'furnitures_out', 'section' => 'furnitures_out'])

@section('content')
    @include('alerts.success')
    <div class="row">
        <div class="col-md-12">
            <div class="card ">
                <div class="card-header">
                    <div class="row">
                        <div class="col-8">
                            <h4 class="card-title"><strong>Furnitures Request</strong></h4>
                            <form action="{{ route('furnitures_out.index') }}" method="GET">
                                <input class="pl-lg-4" type="text" name="search" placeholder="Search Stock No." required/>
                                <button type="submit" class="btn btn-sm btn-secondary">Search</button>
                                <a href="{{ route('furnitures_out.index') }}" class="btn btn-sm btn-secondary">Refresh</a>
                            </form>
                        </div>
                        <div class="col-4 text-right">
                        @if(Auth::user()->role == 'admin')
                            <a href="{{ route('furnitures_manage.index') }}" class="btn btn-sm btn-primary">Manage</a>
                            <a href="{{ route('furnitures_out.create') }}" class="btn btn-sm btn-primary">Stock-out</a>
                            <!-- <a href="{{ 'api/books_in/pdf' }}" class="btn btn-sm btn-primary">Print</a> -->
                        @endif
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    
                    <div class="">
                        <table class="table tablesorter " id="">
                            <thead class=" text-primary">
                                <th scope="col">Inventory Stock No.</th>
                                <th scope="col">Description</th>
                                <th scope="col">Unit</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Date Out</th>
                                <th scope="col">Received By</th>
                                <th scope="col">Transferred To</th>
                            </thead>
                            <tbody>
                                @foreach ($furnitures_out as $item)
                                    <tr>
                                        <td>{{ $item->ManageItems->inventory_stock_no??'' }}</td>
                                        <td>{{ $item->description??'' }}</td>
                                        <td>{{ $item->unit??'' }}</td>
                                        <td>{{ $item->quantity??'' }}</td>
                                        <td>{{ $item->date_out??'' }}</td>
                                        <td>{{ $item->received_by??'' }}</td>
                                        <td>{{ $item->transferred_to??'' }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
               </div>
                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">
                       {{ $furnitures_out->links() }}
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection
