@extends('layouts.app', ['page' => __('Ict Equipment'), 'pageSlug' => 'ict_equips_manage', 'section' => 'transactions'])

@section('content')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Ict Equipment') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('ict_equips_manage.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('ict_equips_manage.update', $ict_equips_manage) }}" autocomplete="off">
                            @csrf
                            @method('put')

                            <h6 class="heading-small text-muted mb-4">{{ __('Ict Equipment Information') }}</h6>
                                <div class="form-group{{ $errors->has('inventory_stock_no') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-inventory_stock_no">Inventory Stock No.</label>
                                    <input type="number" name="inventory_stock_no" id="input-inventory_stock_no" class="form-control form-control-alternative{{ $errors->has('inventory_stock_no') ? ' is-invalid' : '' }}" value="{{ old('inventory_stock_no',$ict_equips_manage->inventory_stock_no) }}" readonly required autofocus>
                                    @include('alerts.feedback', ['field' => 'inventory_stock_no'])
                                </div>
                                <div class="form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-description">Description</label>
                                    <input type="text" name="description" id="input-description" class="form-control form-control-alternative{{ $errors->has('description') ? ' is-invalid' : '' }}" value="{{ old('description',$ict_equips_manage->description) }}" required autofocus>
                                    @include('alerts.feedback', ['field' => 'description'])
                                </div>   
                                <div class="form-group{{ $errors->has('unit') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-unit">Unit</label>
                                    <input type="number" name="unit" id="input-unit" class="form-control form-control-alternative{{ $errors->has('unit') ? ' is-invalid' : '' }}" value="{{ old('unit',$ict_equips_manage->unit) }}" required autofocus>
                                    @include('alerts.feedback', ['field' => 'unit'])
                                </div>   
                                <div class="form-group{{ $errors->has('date_in') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-date_in">Date In</label>
                                    <input type="text" name="date_in" id="input-date_in" class="form-control form-control-alternative{{ $errors->has('date_in') ? ' is-invalid' : '' }}" value="{{ old('date_in',$ict_equips_manage->date_in) }}" required autofocus disabled>
                                    @include('alerts.feedback', ['field' => 'date_in'])
                                </div>   
                                <div class="form-group{{ $errors->has('stock_on_hand') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-stock_on_hand">Stock On Hand</label>
                                    <input type="number" name="stock_on_hand" id="input-stock_on_hand" class="form-control form-control-alternative{{ $errors->has('stock_on_hand') ? ' is-invalid' : '' }}" value="{{ old('stock_on_hand',$ict_equips_manage->stock_on_hand) }}" required autofocus disabled>
                                    @include('alerts.feedback', ['field' => 'stock_on_hand'])
                                </div>
                                <div class="form-group{{ $errors->has('date_out') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-date_out">Date Out</label>
                                    <input type="text" name="date_out" id="input-date_out" class="form-control form-control-alternative{{ $errors->has('date_out') ? ' is-invalid' : '' }}" value="{{ old('date_out',$ict_equips_manage->date_out) }}" required autofocus disabled>
                                    @include('alerts.feedback', ['field' => 'date_out'])
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
