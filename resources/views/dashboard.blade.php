@extends('layouts.app', ['pageSlug' => 'dashboard', 'page' => 'Dashboard', 'section' => ''])

@section('content')
    

    <div class="row">
    <div class="col-md-12">
            <div class="card card-tasks">
                <div class="card-header">
                    <div class="row">
                        <div class="col-8">
                            <h4 class="card-title"></h4>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-full-width table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Inventory Stock No.</th>
                                    <th>Description</th>
                                    <th>Unit</th>
                                    <th>Date In</th>
                                    <th>Stock On Hand</th>
                                    <th>Date Out</th>
                                    <th>Type</th>
                                    <th>

                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($items as $item)
                                    <tr>
                                        <td>{{ $item->inventory_stock_no }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>{{ $item->unit }}</td>
                                        <td>{{ $item->date_in }}</td>
                                        <td>{{ $item->stock_on_hand }}</td>
                                        <td>{{ $item->date_out }}</td>
                                        <td>{{ $item->type }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card card-tasks">
                <div class="card-header">
                    <div class="row">
                        <div class="col-8">
                            <h4 class="card-title">Stock-in</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-full-width table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Inventory Stock No.</th>
                                    <th>Description</th>
                                    <th>Unit</th>
                                    <th>Quantity</th>
                                    <th>Date In</th>
                                    <th>Received By</th>
                                    <th>Transferred To</th>
                                    <th>Type</th>
                                    <th>

                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($stockin as $item)
                                    <tr>
                                        <td>{{ $item->ManageItems->inventory_stock_no??'' }}</td>
                                        <td>{{ $item->description??'' }}</td>
                                        <td>{{ $item->unit??'' }}</td>
                                        <td>{{ $item->quantity??'' }}</td>
                                        <td>{{ $item->date_in??'' }}</td>
                                        <td>{{ $item->received_by??'' }}</td>
                                        <td>{{ $item->transferred_to??'' }}</td>
                                        <td>{{ $item->type??'' }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card card-tasks">
                <div class="card-header">
                <div class="row">
                        <div class="col-8">
                            <h4 class="card-title">Stock-out</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-full-width table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Inventory Stock No.</th>
                                    <th>Description</th>
                                    <th>Unit</th>
                                    <th>Quantity</th>
                                    <th>Date Out</th>
                                    <th>Received By</th>
                                    <th>Transferred To</th>
                                    <th>Type</th>
                                    <th>

                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                            @foreach ($stockout as $item)
                                    <tr>
                                        <td>{{ $item->ManageItems->inventory_stock_no??'' }}</td>
                                        <td>{{ $item->description??'' }}</td>
                                        <td>{{ $item->unit??'' }}</td>
                                        <td>{{ $item->quantity??'' }}</td>
                                        <td>{{ $item->date_out??'' }}</td>
                                        <td>{{ $item->received_by??'' }}</td>
                                        <td>{{ $item->transferred_to??'' }}</td>
                                        <td>{{ $item->type??'' }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="transactionModal" tabindex="-1" role="dialog" aria-labelledby="transactionModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Transaction</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="d-flex justify-content-between">
                        <a href="{{ route('transactions.create', ['type' => 'payment']) }}" class="btn btn-sm btn-primary">Payment</a>
                        <a href="{{ route('transactions.create', ['type' => 'income']) }}" class="btn btn-sm btn-primary">Income</a>
                        <a href="{{ route('transactions.create', ['type' => 'expense']) }}" class="btn btn-sm btn-primary">Expense</a>
                        <a href="{{ route('sales.create') }}" class="btn btn-sm btn-primary">Sale</a>
                        <a href="{{ route('transfer.create') }}" class="btn btn-sm btn-primary">Transfer</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('assets/js/plugins/chartjs.min.js') }}"></script>
    
    <script>
        var lastmonths = [];

        @foreach ($lastmonths as $id => $month)
            lastmonths.push('{{ strtoupper($month) }}')
        @endforeach

        var lastincomes = {{ $lastincomes }};
        var lastexpenses = {{ $lastexpenses }};
        var anualsales = {{ $anualsales }};
        var anualclients = {{ $anualclients }};
        var anualproducts = {{ $anualproducts }};
        var methods = [];
        var methods_stats = [];

        @foreach($monthlybalancebymethod as $method => $balance)
            methods.push('{{ $method }}');
            methods_stats.push('{{ $balance }}');
        @endforeach

        $(document).ready(function() {
            demo.initDashboardPageCharts();
        });
    </script>
@endpush
