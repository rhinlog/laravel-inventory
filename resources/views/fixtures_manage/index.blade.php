@extends('layouts.app', ['page' => 'Manage Fixtures', 'pageSlug' => 'fixtures_manage', 'section' => 'fixtures_manage'])

@section('content')
    @include('alerts.success')
    <div class="row">
        <div class="col-md-12">
            <div class="card ">
                <div class="card-header">
                    <div class="row">
                        <div class="col-8">
                            <h4 class="card-title"><strong>Manage Fixtures</strong></h4>
                            <form action="{{ route('fixtures_manage.index') }}" method="GET">
                                <input class="pl-lg-4" type="text" name="search" placeholder="Search Stock No." required/>
                                <button type="submit" class="btn btn-sm btn-secondary">Search</button>
                                <a href="{{ route('fixtures_manage.index') }}" class="btn btn-sm btn-secondary">Refresh</a>
                            </form>
                        </div>
                       
                        <div class="col-4 text-right">
                        @if(Auth::user()->role == 'admin')
                            <a href="{{ route('fixtures_in.index') }}" class="btn btn-sm btn-primary">Stock-in</a>
                            <a href="{{ route('fixtures_out.index') }}" class="btn btn-sm btn-primary">Stock-out</a>
                            <a href="{{ route('fixtures_manage.create') }}" class="btn btn-sm btn-primary">Add</a>
                        @endif
                            <a href="{{ 'api/fixtures_manage/pdf' }}" class="btn btn-sm btn-primary">Print</a>
                           
                        </div>
                       
                    </div>
                </div>
                <div class="card-body">
                    
                    <div class="">
                        <table class="table tablesorter " id="">
                            <thead class=" text-primary">
                                <th scope="col">Inventory Stock No.</th>
                                <th scope="col">Description</th>
                                <th scope="col">Unit</th>
                                <th scope="col">Date In</th>
                                <!-- <th scope="col">Quantity</th> -->
                                <th scope="col">Stock On Hand</th>
                                <th scope="col">Date Out</th>
                                <th scope="col"></th>
                            </thead>
                            <tbody>
                                @foreach ($fixtures_manage as $item)
                                    <tr>
                                        <td>{{ $item->inventory_stock_no }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>{{ $item->unit }}</td>
                                        <td>{{ $item->date_in }}</td>
                                        <!-- <td>{{ $item->quantity }}</td> -->
                                        <td>{{ $item->stock_on_hand }}</td>
                                        <td>{{ $item->date_out }}</td>
                                        <td class="td-actions text-right">
                                        @if(Auth::user()->role == 'admin')
                                            <a href="{{ route('fixtures_manage.edit', $item) }}" class="btn btn-link" data-toggle="tooltip" data-placement="bottom" title="Edit Item">
                                                <i class="tim-icons icon-pencil"></i>
                                            </a>
                                        @endif
                                            <!-- <form action="{{ route('fixtures_manage.destroy', $item) }}" method="post" class="d-inline">
                                                @csrf
                                                @method('delete')
                                                <button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="bottom" title="Delete Item" onclick="confirm('Are you sure you want to remove this checklist?') ? this.parentElement.submit() : ''">
                                                    <i class="tim-icons icon-simple-remove"></i>
                                                </button>
                                            </form> -->
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">
                       {{ $fixtures_manage->links() }}
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection
