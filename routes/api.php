<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// files
Route::resource('files', 'FilesController');
Route::get('files/{uuid}/download', 'FilesController@download')->name('files.download');

Route::get('ict_equips_manage/pdf', 'PdfController@ict_equips_manage');
Route::get('furnitures_manage/pdf', 'PdfController@furnitures_manage');
Route::get('foods_manage/pdf', 'PdfController@foods_manage');
Route::get('books_manage/pdf', 'PdfController@books_manage');
Route::get('tools_manage/pdf', 'PdfController@tools_manage');
Route::get('office_supplies_manage/pdf', 'PdfController@office_supplies_manage');
Route::get('fixtures_manage/pdf', 'PdfController@fixtures_manage');
Route::get('new_inventory/pdf', 'PdfController@new_inventory');
Route::get('cic_inventory/pdf', 'PdfController@cic_inventory');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
